# PROJET 10

**PROJET 10 - AMELIOREZ LE SYSTEME D’INFORMATION DE LA BIBLIOTHEQUE**

**CONTEXTE**

Vous avez développé le nouveau système d'information pour la gestion des bibliothèques d'une grande ville. Fort du succès rencontré par cette première version, le service culturel reprend contact avec vous afin d'y apporter quelques évolutions et de corriger quelques dysfonctionnements.

 Afin de vous indiquer les modifications à apporter, le client a ouvert des tickets sur votre projet.

Les tickets seront ajoutés par votre mentor. Référez-vous à la section "Travail demandé" pour plus de détails.
Vous n'avez toujours pas à développer les applications mobiles et l'application dédiée au personnel des bibliothèques. Votre projet SoapUI servira à simuler les appels au web service de gestion que feraient ces applications.

**Contraintes**

Vous respecterez les mêmes contraintes que lors du projet de développement initial de ce système d'information.

En voici un rappel :

Le déploiement du système sera assuré par le personnel de la direction des systèmes d'information de la ville. Vous devez donc leur laisser la possibilité de modifier facilement les différents paramètres de configuration (URL et identifiant/mot de passe de la base de données, URL du webservice, envoi des mails...).
L'application web est les batchs n'accèdent pas à la base de données, tout passe par le web service de gestion.

**TRAVAIL DEMANDE**

Pour démarrer ce projet, vous vous baserez sur les livrables que vous avez réalisés lors du projet Développez le nouveau système d’information de la bibliothèque d’une grande ville.
N'écrasez pas votre travail effectué sur le projet précédent, il est nécessaire de ne pas le modifier car il sera consulté par le jury.

Vous en ferez une copie qui servira de point de départ pour ce projet. Le plus réaliste et élégant serait de faire un fork de votre projet sur GitHub ou GitLab...

**Avant de commencer**

    1. Vous créerez le dépôt Git de ce projet en dupliquant celui du projet initial (par copie ou fork).
    2. Si nécessaire, vous donnerez les autorisations nécessaires à votre mentor sur votre nouveau dépôt Git pour qu'il puisse :
        o Consulter votre dépôt (droit de lecture)
        o Créer des tickets/issues
    3. N'allez pas plus loin et prenez contact avec votre mentor !

Pour le bon déroulé de ce projet et une mise en situation plus réaliste, ne commencez pas votre travail tout de suite. En effet, votre mentor va devoir créer les tickets envoyés par votre client sur votre projet Git. Une fois ceci fait, ce sera à vous de jouer !

**Votre mission**

Vous devez dans ce projet :

    • Gérer les tickets ouverts par le client
    • Modifier le code source du système ainsi que la base de données afin de prendre en compte les évolutions demandées par le client (application web et batchs)
    • Mettre à jour le projet SoapUI relatif au web service de gestion pour prendre en compte les changements que vous aurez effectués
    • Ajouter au projet SoapUI les plans de tests relatifs aux nouvelles fonctionnalités et adapter les existants aux modifications apportées au web service
    • Fournir les scripts SQL modifiés pour la création de la base de données
    • Fournir des scripts SQL de migration de la base de données pour la mise en production de la nouvelle version du système
    • Mettre à jour la documentation du projet (MPD, spécifications, fichier README.md...)
 
Les différents composants seront packagés avec Maven :

    • Le web service : au format WAR, et un ZIP des fichiers de configuration nécessaires
    • L’application web ouverte aux usagers : au format WAR, et un ZIP des fichiers de configuration nécessaires
    • Les batchs : au format JAR exécutable ainsi qu'un fichier ZIP contenant le JAR exécutable, les dépendances Java nécessaires, les fichiers de configuration, un script shell (sh ou bash) de lancement par batch
 
**Gestion des tickets**

Vous devrez gérer les tickets ouverts par votre client. Pour ce faire, vous suivrez le processus suivant :

    1. Lors de l'ouverture d'un ticket, vous devrez le qualifier (l'annoter en tant que bug, évolution, nouvelle fonctionnalité...).
    2. Assignez ce ticket à un développeur (vous).
    3. Traitez le ticket en apportant toutes les modifications nécessaires dans le code de l'application par exemple. Cela peut s'étaler sur plusieurs commits. Vous indiquerez la référence du ticket dans chaque commit en rapport avec celui-ci en utilisant la syntaxe spécifique à votre système Git (GitHub, GitLab...).
    4. Sur votre dernier commit, vous mettrez dans le message du commit la commande pour clôturer le ticket en utilisant la syntaxe adéquate.
    5. Vous indiquerez quelle version embarque la correction dans le ticket.

**Utilisation de Git**

Votre code doit être géré avec Git et ce, dès le début du projet. Vous aurez donc un historique assez important sur votre dépôt Git. Votre mentor et le mentor de soutenance pourront consulter cet historique dans le but non pas de relever vos erreurs, mais plutôt de voir votre démarche de développement et votre utilisation de Git.
N'ayez donc pas peur de faire des commits ! Au contraire, un historique contenant seulement quelques "gros" commits révèle souvent une mauvaise approche de développement et une utilisation peu efficace de Git.

Vous serez évalué sur votre utilisation de Git pendant ce projet.

Vous intervenez sur un système qui est déjà déployé en production. Pensez donc que vous pourriez avoir à gérer plusieurs versions parallèle (ex : correction d'un bug sur la version actuelle pendant le développement de la nouvelle version...). Il serait donc judicieux de vous appuyer sur la puissance de Git et notamment sur sa gestion des branches.

**LIVRABLES ATTENDUS**

Vous livrerez, sur GitHub ou GitLab (dans un seul dépôt Git dédié) :

    • Le code source de l’application modifié
    • Les scripts SQL de création de la base de données avec un jeu de données de démo
    • Les scripts SQL de migration de la base de données pour passer à la nouvelle version
    • Les fichiers de configuration exemple
    • Le WSDL du web service ainsi qu’un projet SoapUI contenant un plan de test du web-service
    • Les ressources nécessaires à la création des images Docker des différents composants du système (base de données, web service, application web, batch) ainsi que les fichiers de configuration de docker-compose permettant de déployer :
        o le web service (base de données + web service)
        o l’application web (base de données + web service + application web)
        o le batch (base de données + web service + batch)
    • Une documentation succincte (un fichier  README.mdsuffit) expliquant comment construire les images Docker depuis les sources et déployer l'application
    • La documentation mise à jour (MPD, spécifications...)

